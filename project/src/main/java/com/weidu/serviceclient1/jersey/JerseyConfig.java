package com.weidu.serviceclient1.jersey;

import com.weidu.serviceclient1.resource.AuthorResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * Created by chen on 18/5/21.
 */
@Component
public class JerseyConfig extends ResourceConfig{

    public JerseyConfig() {
        this.register(AuthorResource.class);
    }
}
