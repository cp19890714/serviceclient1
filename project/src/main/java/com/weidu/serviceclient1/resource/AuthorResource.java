package com.weidu.serviceclient1.resource;

import com.weidu.serviceclient1.api.rest.AuthorApi;
import com.weidu.serviceclient1.entity.Author;
import org.springframework.stereotype.Component;

/**
 * Created by chen on 18/5/23.
 */
@Component
public class AuthorResource implements AuthorApi {

    private Author author = new Author("陈鹏 from hello1","删库专家");

    @Override
    public Author getOne() {
        return author;
    }

    @Override
    public String name() {
        return author.getRealName();
    }
}
