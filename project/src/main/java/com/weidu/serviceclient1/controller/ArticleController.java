package com.weidu.serviceclient1.controller;

import com.weidu.serviceclient1.api.springmvc.ArticleApi;
import com.weidu.serviceclient1.entity.Article;
import com.weidu.serviceclient1.entity.Author;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

/**
 * Created by chen on 18/5/23.
 */
@Controller
public class ArticleController implements ArticleApi{

    private Article article = new Article("MySql从删库到跑路", new Author("陈鹏","删库专家"));

    @Override
    public Article getOne() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return article;
    }

    @Override
    public String getTitle() {
        return article.getTitle();
    }

}
