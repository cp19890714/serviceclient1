package com.weidu.serviceclient1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.serviceregistry.EurekaRegistration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by chen on 18/5/21.
 */
@RestController
public class HelloController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EurekaRegistration eurekaRegistration;

    @GetMapping("hello")
    public String index(){
        logger.info("/hello, host:{}, port:{} ",eurekaRegistration.getHost(), eurekaRegistration.getPort());
        return "hello world";
    }

}
