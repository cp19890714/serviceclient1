package com.weidu.serviceclient1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by chen on 18/5/23.
 *
 * 用来读取本项目的api以及feignclient配置,配置文件的 路径+文件名 不可与其他项目一样
 */

@Configuration
@PropertySource(value= "classpath:com/weidu/serviceclient1/config/apiconfig.properties")
public class Hello1ApiConfig {

}
