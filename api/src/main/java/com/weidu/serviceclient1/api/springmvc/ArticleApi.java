package com.weidu.serviceclient1.api.springmvc;

import com.weidu.serviceclient1.entity.Article;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chen on 18/5/23.
 */

@FeignClient(name="hello1springmvc", url="${hello1.name}")
//@FeignClient(name="hello1", configuration = SpringMVCFeignConfiguration.class)
@RequestMapping("/article")
public interface ArticleApi {

    @RequestMapping("/getone")
    @ResponseBody
    Article getOne();

    @RequestMapping("/title")
    @ResponseBody
    String getTitle();

}
