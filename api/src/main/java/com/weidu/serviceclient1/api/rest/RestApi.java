package com.weidu.serviceclient1.api.rest;

import com.weidu.framework.feign.JAXRSFeignConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by chen on 18/5/24.
 */
@FeignClient(name="hello1rest", configuration = JAXRSFeignConfiguration.class)
public interface RestApi {
}
