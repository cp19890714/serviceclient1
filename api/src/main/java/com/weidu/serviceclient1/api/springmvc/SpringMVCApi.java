package com.weidu.serviceclient1.api.springmvc;

import com.weidu.framework.feign.SpringMVCFeignConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by chen on 18/5/24.
 */
@FeignClient(name="hello1springmvc", configuration = SpringMVCFeignConfiguration.class)
public interface SpringMVCApi {
}
