package com.weidu.serviceclient1.api.rest;

import com.weidu.serviceclient1.entity.Author;
import org.springframework.cloud.netflix.feign.FeignClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by chen on 18/5/23.
 */
@FeignClient(name="hello1rest", url="${hello1.name}", path = "${hello1.restprefix}")
//@FeignClient(name="http://hello1", path = "rest", configuration = JAXRSFeignConfiguration.class)
@Path("author")
public interface AuthorApi {

    @GET
    @Produces(value=MediaType.APPLICATION_JSON)
    Author getOne();

    @GET
    @Path("/realname")
    String name();
}
