package com.weidu.serviceclient1.entity;

/**
 * Created by chen on 18/5/23.
 */
public class Author {
    private String realName;
    private String nickName;

    public Author(String realName, String nickName) {
        this.realName = realName;
        this.nickName = nickName;
    }

    public Author() {
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
